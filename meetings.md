## 12.11.19

### Обсуждалось
 - Параллелки, а не распреды
 - Смотреть, как улучшить фреймворк. ЛОВИТЬ БОЛЬШЕ БАГОВ
 - Изучить фреймворк, чтобы понимать все
 - Попросить ссылки про статьи (как писать, читать)
 - Завести репу с ссылками, видео, мыслями, вопросами
 - Конспектировать статьи
 - Выделять ~неделю на конкретную тему

## 21.11.19

FaultInjection + оптимизации из МС

Fuzzing - +Scheduler

MC

модели памяти - более слабые

лок-фри - специфичное для него

линеаризуемость - доклад на Гидре

статический анализ - можно что угодно запихать 


имена атомиков - искать в трейсах, полезны / макросы / 

Folly / Arcadia / проч библ. про трейсы(?)

парсинг программы

макросы

Что обсуждалось:

2 ссылки про написание статей (потихоньку в бэкграунде читать)

Есть рэндж [FaultInjection, Fuzzing, Model Checking].

Первое уже есть, но можно доработать (вопрос - как?)

Второе легче третьего. Надо выяснить, можно ли подружить фаззинг с переключениями контекста.

Я думала, что MC из-за специфики языка стоит делать исключительно с black-box техникой. 
Оказалось, вполне можно внедрять и выделение модели, так как программы у нас достаточно простые, не используют внешние библиотеки. 
Конкретный пример: аннотировать все атомики. 

Строить трейсы: есть проблема с идентификацией одного и того же атомика. Можно статически добавить имя для каждого. Можно даже всю строчку кода! Желательно еще номер строки.

## 2.12.19

хэшировать стеки + использовать свой аллокатор

проверить (пример философов)

стеки потоков + хипы

можно даже не форкаться, а копировать память (стеки + хипа где нужно)

Обходить граф по вариантам, где вставлять/не вставлять FaultInjection. Или, + к этому перебирать все файберы, которые можно. 

2 вещи:
1. фаззинг (хотя бы запустить)
2. подумать про хэширование стеков, модел чекер который мы обсуждали.

