# [Automatic fuzzy-scheduling of threads in Google Thread Sanitizer to detect errors in multithreaded code](http://ceur-ws.org/Vol-2344/paper9.pdf)

Статья о thread scheduling + fuzzing. Хочется ловить дата рейсы. Уже есть Relacy Race Detector (RRD). 

Проблемы RRD:
 - Нельзя динамически изменять кол-во потоков.
 - Структурные проблемы (??)
 - Не работает с TLS.
 - Глобальная память.

По мнению авторов: из-за проблем и непопулярности, вместо RRD лучше использовать TSAN и прикрутить к нему планировщик. 
