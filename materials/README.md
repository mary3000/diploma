# Materials

## MC

### Summaries

 - [Runtime Model Checking of Multithreaded C/C++ Programs](materials/inspect-2008.md)
 - [Minimizing Faulty Executions of Distributed Systems](materials/min-executions-distsys.md)
 - [Relacy Race Detector](materials/relacy-race-detector.md)
 - [Teaching Rigorous Distributed Systems With Efficient Model Checking](materials/teaching-distsys-mc.md)
 - [CMC: a pragmatic approach to model checking real code](materials/cmc.md)
 - [Model Checking TLA+ Specifications](materials/mc-tla-spec.md)

### Refs
 - [Partial-Order Methods for the Verification of Concurrent Systems](https://pdfs.semanticscholar.org/acaf/9c18e5711d4fd63144953292d43fb3fdac92.pdf). Много теории о POR.
 - [Dynamic Partial-Order Reduction for Model Checking Software](https://users.soe.ucsc.edu/~cormac/papers/popl05.pdf) Теория о DPOR.
 - [Effective Stateless Model Checking for C/C++ Concurrency](https://plv.mpi-sws.org/rcmc/paper.pdf)
 - [Program Model Checking. A Practitioner’s Guide](https://pdfs.semanticscholar.org/122d/f6d37f29dcccb2ce8287e19e0e341d3d0e59.pdf) Много. М.б. есть что-то полезное. 

## Fuzzing

### Summaries
 - [C++ Russia 2017: Дмитрий Вьюков, Fuzzing: The New Unit Testing [video]](materials/fuzzing-vyukov.md)
 - [Automatic fuzzy-scheduling of threads in Google Thread Sanitizer to detect errors in multithreaded code](materials/tsan-fuzzy-scheduling.md)

### Refs
 - [Твит от @dvyukov](https://twitter.com/dvyukov/status/1153296235014631431)
 - [Cuzz](https://blog.poisson.chat/posts/2017-10-06-cuzz.html)
 

## Linearizability

### Summaries
 - [Lin-Check [video]](https://gitlab.com/mary3000/diploma/blob/master/materials/lincheck.md)

### Refs
 - [Finding Linearization Violations in Lock-Free. Concurrent Data Structures](https://pdos.csail.mit.edu/papers/codex_linearization_violations.pdf)