# Relacy Race Detector

[Intro](http://www.1024cores.net/home/relacy-race-detector/rrd-introduction), [github](https://github.com/dvyukov/relacy)

Прообраз ThreadSanitizer'а?

Непопулярный (почему?). Тестирует (немного) измененный С++ код, поддерживает слабые модели памяти. Умеет: 
>  checks for data races, deadlocks, livelocks, accesses to freed memory, double memory free, memory leaks, accesses to uninitialized variables, incorrect API usage (recursive acquisition of non-recursive mutex), as well as verification of user specified asserts and invariants.

Есть трейсы:
>  it will output detailed execution history that leads to the error (history includes such things as instances of the ABA problem, reorderings of memory accesses, thread blocking/unblocking)

Планировщики:
 - random scheduler
 - full search scheduler 
 - context bound scheduler
