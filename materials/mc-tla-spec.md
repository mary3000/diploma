# [Model Checking TLA+ Specifications](https://lamport.azurewebsites.net/pubs/yuanyu-model-checking.pdf)

> Despite my warning him that it would be impossible, Yuan Yu wrote a model checker for TLA+ specifications. He succeeded beyond my most optimistic hopes. This paper is a preliminary report on the model checker. <...>

(c) [Leslie Lamport](http://lamport.azurewebsites.net/pubs/pubs.html#yuanyu-model-checking)

---
 
 TLA+ изначально написали для спецификации и построения доказательств систем, а не модел чекинга.
 
 TLC нужен скорее, чтобы находить ошибки спецификации, а не для полной верификации модели. Значит, скорость чекера не так важна.
 
 Написан на Java.
 
 Хранит все на диске, память - как кэш.

### Как работает TLC

 Есть сет `seen` - чексуммы посещенных состояний.
 
 Есть очередь `sq` - подмножество `seen`. Это те состояния, потомки которых еще нужно посетить.
 
 В `seen` хранятся отпечатки предков, чтобы в конце сгенерировать последовательность чексумм. Для создания трейса чекер перезапускает нужный путь с начала.
 
 Попытки компактно представить состояния провалились. IO итак хорошо утилизируется, посколько структура данных - очередь. Место сокращается не сильно, а считать компактное представление долго.
 
 `seen` хранится:
   - Отсортированно на диске.
   - Хэш-таблицей в памяти. Она сбрасывается на диск: сортируется и сливается с тем, что уже есть.